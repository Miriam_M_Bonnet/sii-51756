// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"
#include "Esfera.h"
class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	Esfera disparo;
	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
