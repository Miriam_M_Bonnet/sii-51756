#pragma once
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{
public:
	Esfera esfera;
	Raqueta raqueta1;
	Raqueta raqueta2;
	int accion1;//1 arriba, 0 nada, -1abajo
	int accion2;
	void set_accion1 ()
	{
		float medio=(raqueta1.y2+raqueta1.y1)/2.0f;
		if (esfera.centro.y>medio)
			accion1=1;
		else
			accion1=-1;
		return ;
	};
	void set_accion2 ()
	{
		float medio=(raqueta2.y2+raqueta2.y1)/2.0f;
		if (esfera.centro.y>medio)
			accion2=1;
		else
			accion2=-1;
		return ;
	};
};
