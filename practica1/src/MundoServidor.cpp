// Mundo.cpp: implementation of the CMundo class.
//Miriam Moussadak 51756
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <sys/wait.h>

#define TAM_CAD 150
#define T_SIN_JUGAR 10.0F
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void *hilo_comandos(void * arg)
{
	CMundoServidor *p=(CMundoServidor*)arg;
	p->RecibeComandosJugador();
}

CMundoServidor::CMundoServidor()
{

	Init();
}

CMundoServidor::~CMundoServidor()
{
//Comunicamos a logger que termina el juego para que termine
	const char *fin="Fin del juego\n";
	if(-1 == write (fd,fin,TAM_CAD))
	    {
		perror("write(fifo_logger,fin del juego)");
		exit(EXIT_FAILURE);
	    }

	if(-1 == close (fd))
	    {
		perror("close(fifo_logger) en ~CMundoServidor()");
		exit(EXIT_FAILURE);
	    }

	if(-1 == close (fd_fifo2))
	    {
		perror("close(fifo2) en ~CMundoServidor()");
		exit(EXIT_FAILURE);
	    }

	if(-1 == close (fd_fifo3))
	    {
		perror("close(fifo3) en ~CMundoServidor()");
		exit(EXIT_FAILURE);
	    }

}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}


void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		char cad[TAM_CAD];
		sprintf(cad, "Jugador 2 marca 1 punto, tiene un total de %d\n",puntos2);
		if(-1==write (fd,cad,TAM_CAD))
		{
			perror("write(fifo_logger) en MundoServidor::OnTimer");
			exit(EXIT_FAILURE);
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		char cad[TAM_CAD];
		sprintf(cad, "Jugador 1 marca 1 punto, tiene un total de %d\n",puntos1);
		if(-1 == write (fd,cad,TAM_CAD))
			    {
				perror("write(fifo_logger) en MundoServidor::OnTimer()");
				exit(EXIT_FAILURE);
			    }
	}

	char buf_2[TAM_CAD];
	if(0>sprintf(buf_2,"%f %f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x,esfera.centro.y,esfera.radio,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2))
	{
		perror("sprintf");
		exit(EXIT_FAILURE);
	}
	
	if(-1 == write(fd_fifo2,&buf_2,TAM_CAD))
	    {
		perror("write(fifo2) en MundoServidor::OnTimer()");
		exit(EXIT_FAILURE);
	    }
	t_sinJugar+=0.025f;
	
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
	if(y!=1) t_sinJugar=0;
//al llamar a este método desde OnTimer se habrá hecho y=1 para diferenciar las llamadas del usuario real de las del bot
}

void CMundoServidor::RecibeComandosJugador()
{
	while(1)
	{
		usleep(10);
		char buf;
		int rd=read(fd_fifo3,&buf,1);
		if(rd==-1){
			perror("read RecibeComandos");
			exit(EXIT_FAILURE);
		}

		unsigned char key;
		sscanf(&buf,"%c",&key);

		switch(key)
		{
	//	case 'a':jugador1.velocidad.x=-1;break;
	//	case 'd':jugador1.velocidad.x=1;break;
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
	//	case 'l':jugador2.velocidad.y=-4;break;
	//	case 'o':jugador2.velocidad.y=4;break;
		}
	}
}

void CMundoServidor::Init()
{

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


	t_sinJugar=0;
	
	const char * nombre_fifo="/tmp/fifo_logger";
	fd=open(nombre_fifo,O_RDWR);
	if(-1 == fd)
	    {
		perror("open(fifo_logger) en MundoServidor::Init()");
		exit(EXIT_FAILURE);
	    }

	const char * nombre_fifo2="/tmp/fifo_ClienteServidor";
	fd_fifo2=open(nombre_fifo2,O_WRONLY,0666);
	if(-1==fd_fifo2)
	{
		perror("open(fifo2) en MundoServidor");
		exit(EXIT_FAILURE);
	}

	const char * nombre_fifo3="/tmp/fifo_ClienteServidor2";
	fd_fifo3=open(nombre_fifo3,O_RDONLY,0666);
	if(-1==fd_fifo3)
	{
		perror("open(fifo3) en MundoServidor");
		exit(EXIT_FAILURE);
	}

	pthread_create(&thid1,NULL,hilo_comandos,this);

	
}
