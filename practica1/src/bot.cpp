//Miriam Moussadak 51756
#include <stdio.h>
#include <stdlib.h>

#include "DatosMemCompartida.h"

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

int main(int argc,char* argv[])
{
  const char * fich_bot="/tmp/fich_bot";
  int fd=open(fich_bot,O_RDWR);
  if (-1==fd)
  {
	perror("open(fich_bot) en bot.cpp");
	exit(EXIT_FAILURE);
  }
  int tam=sizeof(DatosMemCompartida);
   DatosMemCompartida* p_datos_bot=static_cast<DatosMemCompartida*>(mmap(NULL,tam,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
  if(p_datos_bot==MAP_FAILED)
		{
	 	    perror("mmap en Mundo.cpp");
		    exit(EXIT_FAILURE);
		}

  if (-1==close (fd))
  {
	perror("close(fich_bot) en bot.cpp");
	exit(EXIT_FAILURE);
  }

  while(p_datos_bot->accion1!=5)
  {
	if(p_datos_bot->esfera.centro.x<-0.5f)
		p_datos_bot->set_accion1();
	if(p_datos_bot->esfera.centro.x>0.5f)
		if(p_datos_bot->accion2==6)
			p_datos_bot->set_accion2();

	usleep(200000);

  }

  if (-1==munmap(p_datos_bot, tam))
    {
        perror("munmap");
        exit(EXIT_FAILURE);
    }

  if(-1 == unlink(fich_bot))
    {
	perror("unlink(fich_bot)");
	exit(EXIT_FAILURE);
    }
  return 0;
}


