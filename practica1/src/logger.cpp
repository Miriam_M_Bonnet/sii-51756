//Miriam Moussadak 51756
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>

#define TAM_BUF 150
int main(int argc,char* argv[])
{
  const char * nombre_fifo = "/tmp/fifo_logger";//int mkfifo(const char *pathname, mode_t mode);
  if(-1 == mkfifo(nombre_fifo,0666))
	    {
		perror("mkfifo");
		exit(EXIT_FAILURE);
	    }

  int fd=open(nombre_fifo,O_RDONLY);
  if(-1 == fd)
	    {
		perror("open(fifo_logger) en logger.cpp");
		exit(EXIT_FAILURE);
	    }

  char buf[TAM_BUF]="Comienza el juego\n";
  while(buf[0]!='F')
  {

	if(-1 == read (fd,&buf,TAM_BUF))
	    {
		perror("read(fifo_logger) en logger.cpp");
		exit(EXIT_FAILURE);
	    }
	
	if(-1 == write(1,&buf,strlen(buf)+1))
	    {
		perror("write(STDOUT) en logger.cpp");
		exit(EXIT_FAILURE);
	    }
  }
//Cerramos el fd y borramos el fichero fifo_logger

  if(-1 == close (fd))
    {
	perror("close(fifo_logger) en logger.cpp");
	exit(EXIT_FAILURE);
    }
  
  if(-1 == unlink(nombre_fifo))
    {
	perror("unlink(fifo_logger) en logger.cpp");
	exit(EXIT_FAILURE);
    }

  return 0;
}
