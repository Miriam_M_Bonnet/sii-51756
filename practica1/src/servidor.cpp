//Miriam Moussadak 51756
#include "glut.h"
#include <string.h>
#include <stdlib.h>
#include "MundoServidor.h"
#include <signal.h>
//el unico objeto global
CMundoServidor mundo;

//los callback, funciones que seran llamadas automaticamente por la glut
//cuando sucedan eventos
//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); //esta funcion sera llamada para dibujar
void OnTimer(int value); //esta funcion sera llamada cuando transcurra una temporizacion
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	
void handle_signal(int sig);

int main(int argc,char* argv[])
{
	//acción frente a señales
	struct sigaction act;
 
	memset (&act, 0, sizeof(act));
	sigemptyset(&act.sa_mask);

	act.sa_handler = &handle_signal;
	act.sa_flags= SA_RESTART;


	if (sigaction(SIGINT, &act, NULL) < 0) {
		perror ("sigaction");
		return 1;
	}
	if (sigaction(SIGTERM, &act, NULL) < 0) {
		perror ("sigaction");
		return 1;
	}
	if (sigaction(SIGPIPE, &act, NULL) < 0) {
		perror ("sigaction");
		return 1;
	}
	if (sigaction(SIGUSR1, &act, NULL) < 0) {
		perror ("sigaction");
		return 1;
	}

	//Inicializar el gestor de ventanas GLUT
	//y crear la ventana
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Servidor");


	//Registrar los callbacks
	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0);//le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundo.InitGL();

	
	//pasarle el control a GLUT,que llamara a los callbacks
	glutMainLoop();	

	return 0;   
}

void OnDraw(void)
{
	mundo.OnDraw();
}
void OnTimer(int value)
{
	mundo.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	mundo.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}


void handle_signal(int sig) 
{
	const char *sig_name;
	int status;
	switch(sig)
	{
		case SIGINT :sig_name="SIGINT" ;status=EXIT_FAILURE;break;
		case SIGPIPE:sig_name="SIGPIPE";status=EXIT_FAILURE;break;
		case SIGTERM:sig_name="SIGTERM";status=EXIT_FAILURE;break;
		case SIGUSR1:sig_name="SIGUSR1";status=EXIT_SUCCESS;break;
		default:status=3;
	}
	if(status!=3){
	printf("Signal %d(%s) terminated process Servidor with value %d\n",sig,sig_name,status);
	exit(status);
	}
	else
	printf("Caught the wrong signal");

}
