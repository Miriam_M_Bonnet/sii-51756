// Mundo.cpp: implementation of the CMundo class.
//Miriam Moussadak 51756
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#define TAM_CAD 150
#define T_SIN_JUGAR 10.0F
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{

	Init();
}

CMundoCliente::~CMundoCliente()
{

//Comunicamos a bot que termina el juego para que termine
	 p_datos->accion1=5;
	 if (munmap(p_datos,sizeof(DatosMemCompartida)) == -1)
	    {
		perror("munmap");
		exit(EXIT_FAILURE);
	    }
//Cerramos fifo2
	 if(-1 == close (fd_fifo2))
    	    {
		perror("close(fifo2) en ~CMundoCliente()");
		exit(EXIT_FAILURE);
   	    }
  
	 if(-1 == unlink(nombre_fifo2))
	    {
		perror("unlink(fifo2) en ~CMundoCliente()");
		exit(EXIT_FAILURE);
	    }
//cerramos fifo3
	if(-1 == close (fd_fifo3))
    	    {
		perror("close(fifo3) en ~CMundoCliente()");
		exit(EXIT_FAILURE);
   	    }
  
	 if(-1 == unlink(nombre_fifo3))
	    {
		perror("unlink(fifo3) en ~CMundoCliente()");
		exit(EXIT_FAILURE);
	    }
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print2(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print2(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print2(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	
	p_datos->esfera=esfera;
	p_datos->raqueta1=jugador1;
	p_datos->raqueta2=jugador2;
	if(p_datos->accion1== 1)OnKeyboardDown('w',0,1);
	if(p_datos->accion1==-1)OnKeyboardDown('s',0,1);
	//if(p_datos->accion2== 1)OnKeyboardDown('o',0,1);
	//if(p_datos->accion2==-1)OnKeyboardDown('l',0,1);


	if(t_sinJugar>T_SIN_JUGAR)
		p_datos->accion2=6;
	else 
		p_datos->accion2=0;
	t_sinJugar+=0.025f;
	
	
	char buf_2[TAM_CAD];
	if(-1==read(fd_fifo2,&buf_2,TAM_CAD))
	{
		perror("read(fifo2) en CMundoCliente::OnTimer");
		exit(EXIT_FAILURE);
	}
	sscanf(buf_2,"%f %f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x,&esfera.centro.y,&esfera.radio,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;
	}

//al llamar a este método desde OnTimer se habrá hecho y=1 para diferenciar las llamadas del usuario real de las del bot
	if(y!=1) t_sinJugar=0;
//mandamos por fifo3 la tecla pulsada
	if(-1==write(fd_fifo3,&key,1))
	{
		perror("write(fifo3) en CMundoCliente::OnKeyboardDown");
		exit(EXIT_FAILURE);
	}
}

void CMundoCliente::Init()
{

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


	t_sinJugar=0;
	
	//Creamos fifo2 y la abrimos
	nombre_fifo2="/tmp/fifo_ClienteServidor";
	if(-1==mkfifo(nombre_fifo2,0666))
	{
		perror("mkfifo(fifo2) en CMundoCliente::Init()");
		exit(EXIT_FAILURE);
	}
	
	fd_fifo2=open(nombre_fifo2,O_RDWR|O_TRUNC,0666);
	if(-1==fd_fifo2)
	{
		perror("open(fifo2) en CMundoCliente::Init()");
		exit(EXIT_FAILURE);
	}
	
	//Creamos fifo3 y la abrimos
	nombre_fifo3="/tmp/fifo_ClienteServidor2";
	if(-1==mkfifo(nombre_fifo3,0666))
	{
		perror("mkfifo(fifo3) en CMundoCliente::Init()");
		exit(EXIT_FAILURE);
	}
	fd_fifo3=open(nombre_fifo3,O_RDWR|O_TRUNC,0666);
	if(-1==fd_fifo3)
	{
		perror("open(fifo3) en CMundoCliente::Init()");
		exit(EXIT_FAILURE);
	}

	//Creamos un fichero como zona de memoria compartida por el bot y el cliente
	int fd_bot=open("/tmp/fich_bot",O_RDWR|O_CREAT|O_TRUNC,0666);
	if(-1 == fd_bot)
	    {
		perror("open(fich_bot) en CMundoCliente::Init()");
		exit(EXIT_FAILURE);
	    }

	//si no guardamos nada en el fichero el acceso generará un sigsegv (realmente no existiría el fichero)
	datos.esfera=esfera;
	datos.raqueta1=jugador1;

	int tam=sizeof(DatosMemCompartida);

	if(-1 == write(fd_bot,&datos,tam))
	    {
		perror("write(fich_bot) en CMundoCliente::Init()");
		exit(EXIT_FAILURE);
	    }

	p_datos=static_cast<DatosMemCompartida*>(mmap(NULL,tam,PROT_READ|PROT_WRITE,MAP_SHARED,fd_bot,0));
	if(p_datos==MAP_FAILED)
		{
	 	    perror("mmap en CMundoCliente::Init()");
		    exit(EXIT_FAILURE);
		}
	if(-1 == close (fd_bot))
	    {
		perror("close(fifo_bot) en CMundoCliente::Init()");
		exit(EXIT_FAILURE);
	    }
	

}
